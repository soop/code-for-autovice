module.exports = {
  apps : [{
    name: 'Foley REST API',
    script: 'server.js',
    autorestart: true,
    instances : "max",
    exec_mode : "cluster"
  }],
};
