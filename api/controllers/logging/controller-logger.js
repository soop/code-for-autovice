const model_logger = require('../../models/logging/model-logger')
const permission = require('../../helpers/permission')
const time = require('../../helpers/time')

/**
 * @obj user: object, action: string, target: string, targetId: string
 */
exports.log = async (obj) => {

  try {

    const query = {
      actionTime: time.now(),
      user: obj.user.name,
      action: obj.action,
      target: obj.target,
      targetId: obj.targetId
    }

    const logEntry = await model_logger.create(query)

    console.log(logEntry)

  } catch (error) {
    console.log(error)
  }
}

/**
 * exports the 250 latest entries
 */
exports.fetchLogs = async (req, res, next) => {

  permission.check('logsView', req, res)

  try {

    const query = {
      company: req.userData.company
    }

    const logs = await model_logger.find(query, null, { limit: 250, sort: { actionTime: -1 } })

    return res.status(200).json({
      status: 200,
      message: `returned array of ${logs.length} logs`,
      data: logs
    })

  } catch (error) {
    console.log(error)
    return res.status(500).json({
      status: 500,
      message: 'Server error'
    })
  }

}

/**
 * exports 2000 lines of logs in the form of a file
 */
exports.exportLogs = async (req, res, next) => {

  permission.check('logsView', req, res)

  try {

    const query = {
      company: req.userData.company
    }

    const logs = await model_logger.find(query, null, { limit: 2000, sort: { actionTime: -1 } })


    let logFile = ''

    for (let i = 0; logs.length > i; i++) {
      logFile += `${logs[i].actionTime} -- ${logs[i].user} -- ${logs[i].action} -- ${logs[i].target} -- ${logs[i].targetId}\n`
    }

    res.set({
      'Content-Disposition': 'attachment; filename=\'user-action-log.txt\''
    })

    // TODO: test if this is downloaded as a file, or if it will display in the browser.
    return res.send(logFile)

  } catch (error) {
    console.log(error)
    return res.status(500).json({
      status: 500,
      message: 'Something went wrong in the export'
    })
  }

}
