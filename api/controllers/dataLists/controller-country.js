const model_country = require('../../models/dataLists/model-country')

exports.listCountries = async (req, res, next) => {

  try {

    const list = await model_country.find()

    if (!list) {
      res.status(500).json({
        status: 500,
        message: 'something went wrong'
      })
    } else {
      res.status(200).json({
        status: 200,
        data: list
      })
    }

  } catch (error) {
    res.status(500).json({
      status: 500,
      message: 'Something went wrong'
    })
  }

}
