const controller_register = require('../auth/controller-register');
const controller_roles = require('../auth/controller-roles');
const controller_user = require('../user/controller-user');
const model_company = require('../../models/administration/model-company');

const venueNumber = require('../../helpers/venueNumber');

const model_venue = require('../../models/administration/model-venue');

const logger = require('../logging/controller-logger');
const crypto = require('crypto')
const geo = require('../../helpers/geo');
const genCustId = require('../../helpers/genCustId')

exports.addCompanyVenue = async (req, res, next) => {

  try {

    const request = req.body;

    const userItem = {
      email: request.email,
      firstName: request.firstName,
      lastName: request.lastName,
      password: await crypto.randomBytes(90).toString('hex')
    }

    console.log(userItem.password);


    const user = await controller_register.companySignupUser(userItem);

    const item = {
      companyName: request.companyName,
      customer_id: request.customer_id,
      cvr: request.cvr,
      adminUser: user._id,
      location: {
        address: request.address,
        zip: request.zip,
        country: request.country,
        region: request.region,
        municipality: request.municipality
      },
      contacts: [
        { user_id: user._id, userName: user.firstName + ' ' + user.lastName }
      ]
    }

    item.customer_id = await genCustId.generate()

    const company = await model_company.create(item);

    // const company = new model_company(item)
    // company.save()

    const role = await controller_roles.createAdmin(company._id);

    if (company) {

      const update = {
        company: company._id,
        $push: {roles: {role: role._id}}
      };

      if (!await controller_user.updateUser({_id: user._id}, update)) {
        return res.status(500).json({
          status: 500,
          message: 'Server error 500'
        });
      }
    }

    await logger.log({
      user: user,
      action: `Create new company`,
      target: `${company.companyName}`,
      targetId: company._id
    })

    const cords = geo.addressToCords(`${request.country}, ${request.region}, ${request.address}`);

    const query = {
      company: company._id,
      name: request.name,
      venueTypes: request.venueTypes,
      customerContact: {
        url: request.url,
        email: request.email,
        phone: request.phone
      },
      location: {
        coordinates: {
          lng: (cords) ? cords[0] : 0,
          lat: (cords) ? cords[1] : 0
        },
        country: request.country,
        address: request.address,
        zip: request.zip,
        city: request.city,
        region: request.region,
        municipality: request.municipality
      },
      about: request.about
    }

    query.venue_number = await venueNumber.generate({ country: query.location.country, zip: query.location.zip, venueTypes: query.venueTypes })

    const venue = await model_venue.create(query);

    await logger.log({
      user: user,
      action: `Create new venue`,
      target: `${venue.name}`,
      targetId: venue._id
    })



    res.status(200).json({
      status: 200,
      message: `successfully created db entry`,
      data: {company, user, role, venue}
    });

  } catch (error) {
    console.log(error);

    return res.status(500).json({
      status: 500,
      message: 'action failed'
    });
  }

}
