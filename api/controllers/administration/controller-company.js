const controller_register = require('../auth/controller-register');
const controller_roles = require('../auth/controller-roles');
const controller_user = require('../user/controller-user');
const model_company = require('../../models/administration/model-company');

const logger = require('../logging/controller-logger');

exports.findOneCompany = async (req, res, next) => {

  try {

    const company = await model_company.find();

    return res.status(201).json({
      status: 201,
      message: `Successfully return a array of ${company.length} items`,
      count: company.length,
      data: company
    })

  } catch (error) {
    console.error(error);
    res.status(500).json({
      status: 500,
      message: `Server error`
    });
  }

};

exports.insertCompany = async (req, res, next) => {

  try {

    const request = req.body;
    const userItem = {
      email: request.email,
      firstName: request.firstName,
      password: request.password
    }

    const user = await controller_register.companySignupUser(userItem);

    const item = {
      companyName: request.companyName,
      customer_id: request.customer_id,
      cvr: request.cvr,
      adminUser: user._id,
      location: {
        address: request.address,
        zip: request.zip,
        country: request.country,
        region: request.region,
        municipality: request.municipal
      },
      contacts: [
        { user_id: user._id, userName: user.name }
      ]
    }

    const company = await model_company.create(item);

    const role = await controller_roles.createAdmin(company._id);

    if (company) {

      const update = {
        company: company._id,
        $push: {roles: {role: role._id}}
      };

      if (!await controller_user.updateUser({_id: user._id}, update)) {
        return res.status(500).json({
          status: 500,
          message: 'Server error 500'
        });
      }
    }

    await logger.log({
      user: user,
      action: `Create new company`,
      target: `${company.companyName}`,
      targetId: company._id
    })

    return res.status(200).json({
      status: 200,
      message: `successfully created db entry`,
      count: company.length,
      data: company
    });

  } catch (error) {
    return res.status(500).json({
      status: 500,
      message: 'Server error 500'
    });
  }

}
