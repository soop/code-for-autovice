const venueNumber = require('../../helpers/venueNumber')

const model_venue = require('../../models/administration/model-venue')

const geo = require('../../helpers/geo')
const permission = require('../../helpers/permission')

exports.findVenue = async (req, res) => {

  try {

    permission.check('venueView', req, res)

    let query = {}

    if (!req.body.id) {
      query = {
        company: req.userData.company,
        isDeleted: false
      }
    } else {
      query = {
        _id: req.body.id,
        company: req.userData.company,
        isDeleted: false
      }
    }

    if (req.userData.permissions.superUser === true) {
      delete query.company
    }

    const venues = await model_venue.find(query, null, { limit: 100 })

    return res.status(201).json({
      status: 201,
      message: 'Successfully returned 1 item',
      data: venues
    })

  } catch (error) {
    console.error(error)
    res.status(500).json({
      status: 500,
      message: 'Server error 500'
    })
  }
}

exports.insertVenue = async (req, res) => {

  permission.check('venueCreate', req, res)

  const request = req.body

  try {

    const cords = geo.addressToCords(`${request.country}, ${request.region}, ${request.address}`)

    const query = {
      company: req.userData.company,
      name: request.name,
      venueTypes: request.venueTypes,
      stage: request.stage,
      customerContact: {
        url: request.url,
        email: request.email,
        phone: request.phone
      },
      location: {
        coordinates: {
          lng: (cords) ? cords[0] : 0,
          lat: (cords) ? cords[1] : 0
        },
        country: request.country,
        address: request.address,
        zip: request.zip,
        city: request.city,
        region: request.region,
        municipality: request.municipal
      },
      about: request.about
    }

    query.venue_number = await venueNumber.generate({zip: query.location.zip, country: query.location.country, venueTypes: query.venueTypes})

    const venue = await model_venue.create(query)

    return res.status(200).json({
      status: 200,
      message: 'successfully created new venue',
      count: venue.length,
      data: venue
    })

  } catch (error) {
    console.error(error)
    res.status(500).json({
      status: 500,
      message: 'Server error 500'
    })
  }
}

exports.delVenue = async (req, res) => {

  permission.check('venueDelete', req, res)

  try {

    const query = {
      _id: req.body.id
    }

    await model_venue.findByIdAndUpdate(query, { isDeleted: true })

    return res.status(200).json({
      status: 200,
      message: 'successfully deleted venue'
    })

  } catch (error) {
    res.status(500).json({
      status: 500,
      message: 'Server error 500'
    })
  }
}
