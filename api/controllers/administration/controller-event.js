const model_event = require('../../models/administration/model-event')
const __ = require('../../helpers/__');
const permission = require('../../helpers/permission')

exports.findOneEvent = async () => {

  try {

    permission.check('venueView', req.userData.permissions, res)

    const query = {
      _id: req.body.id,
      company: req.userData.company
    }

    const event = await model_event.findOne(query)

    return res.status(200).json({
      status: 200,
      message: 'successfully returned 1 item',
      data: event
    })

  } catch (error) {
    console.log(error);
    return res.status(500).json({
      status: 500,
      message: '500 Server error'
    })
  }

}

exports.findAllEvents = async (req, res, next) => {

  try {

    permission.check('venueView', req, res);

    const query = {
      company: req.userData.company,
      venue_id: req.body.venue_id
    }

    const events = await model_event.find(query, null, { limit: 200, sort: { actionTime: -1 } });

    return res.status(200).json({
      status: 200,
      message: 'Successfully returned array of events',
      data: events
    })

  } catch (error) {
    console.log(error);
    return res.status(500).json({
      status: 500,
      message: '500 server error'
    })
  }

}

exports.insertEvent = async (req, res, next) => {

  permission.check('eventCreate', req.userData.permissions, res)

  const request = req.body

  try {

    const query = {
      venue_id: request.venue_id,
      company: req.userData.company,
      title: request.title,
      subdivision: request.subdivision,
      description: request.description,
      duration: request.duration,
      intermission: request.intermission,
      intermissionTime: request.intermissionTime,
      ageLimit: request.ageLimit,
      numberedSeats: request.numberedSeats,
      status: request.status
    }

    if (request.intermission) {
      query.totalPlayingTime = parseInt(request.duration) + parseInt(request.intermissionTime)
    }

    query.genre = (query.genre) ? __.arrayToArrayObject(request.genre, 'genre') : null
    query.credits = (query.credits) ? __.creditsFormat(request.credits) : null

    // TODO: add image uploader to 'bucket' / DO spaces
    // query.images =

    query.category = (query.category) ? __.arrayToArrayObject(request.category, 'category') : null

    const event = await model_event.create(query);

    return res.status(200).json({
      status: 200,
      message: 'successfully created 1 event',
      data: event
    })

  } catch (error) {
    console.log(error);
    return res.status(500).json({
      status: 500,
      message: '500 server error'
    })
  }

}
