const model_roles = require('../../models/auth/model-roles')


exports.permissionTable = async (req, res, next) => {

  try {

    const query = {
      userRoles: req.userData.roles,
    }

    // TODO: Figure out why this isn't working with await
    const roles = await model_roles.find(query)

    console.log(roles)

  } catch (error) {
    console.log(error)
    return res.status(500).json({
      status: 500,
      message: 'Something went wrong, please contact administration at glen@soop.dk'
    })
  }

}

