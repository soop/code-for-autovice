const mailgun = require('../../helpers/mailgun')

const model_conctactMessage = require('../../models/contacts/model-contactMessage');
const model_companySignup = require('../../models/contacts/model-companySignup');

exports.insertContactMessage = async (req, res, next) => {

  try {

    const item = {
      name: req.body.name,
      email: req.body.email,
      subject: req.body.subject,
      message: req.body.message
    }

    const message  = model_conctactMessage.create(item);

    if (message) {

      mailgun.send(null, {
        target: (req.body.email) ? `glen@soop.dk, ${req.body.email}` :`glen@soop.dk`,
        subject: `a contact message has been recieved`,
        text: `contact message mail`
      });

      return res.status(200).json({
        status: 200,
        message: `We have received your message.`
      })
    }

  } catch (error) {
    console.log(error);
    return res.status(500).json({
      status: 500,
      message: `It looks the server encountered an error`
    })
  }

}

exports.insertCompanyContactMessage = async (req, res, next) => {

  try {

    const message = {
      cvr: req.body.cvr,
      companyName: req.body.companyName,
      website: req.body.website,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      phoneNumber: req.body.phoneNumber,
      email: req.body.email
    }

    const company = await model_companySignup.create(message);

    // TODO: make a template for auto answer that we recieved the message.
    if (company) {
      mailgun.send('partnerSignupConfirm', {
        target: (req.body.email) ? `glen@soop.dk.dk, ${req.body.email}` :`glen@soop.dk.dk`,
        subject: `Ansøgning modtaget`,
        text: `Test mail.`,
        params: {name: req.body.firstName, companyName: req.body.companyName}
      })
    }

    return res.status(200).json({
      status: 200,
      message: `We've recieved your message`,
      data: company
    })

  } catch (error) {
    console.log(error);
    return res.status(500).json({
      status: 500,
      message: `It looks like the server encountered an error`
    })
  }

}
