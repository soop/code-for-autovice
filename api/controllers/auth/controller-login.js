require('dotenv').config()
const model_users = require('../../models/user/model-users')

const time = require('../../helpers/time')

exports.login = async (req, res, next) => {

  try {

    const email = req.body.email
    const password = req.body.password

    console.log(email, password)


    if (!email && !password) {
      return res.status(400).json({
        status: 400,
        message: 'Critical information is missing'
      })
    }

    if (password.length < 8) {
      return res.status(400).json({
        status: 400,
        message: 'Critical information is missing'
      })
    }

    const userRes = await model_users.findByCredentials(email, password)

    await model_users.updateOne({email: email}, {lastLogin: time.now()})

    // console.log(userRes);

    if (!userRes) {
      return res.status(401).json({
        status: 401,
        message: 'Login failed, please check credentials'
      })
    }

    const token = await userRes.generateAuthToken()

    userRes.password = '****************'

    return res.status(201).json({
      status: 201,
      message: 'User succesfully logged in',
      user: userRes,
      token: token
    })

  } catch (error) {
    console.error(error)
    res.status(500).json({
      status: 500,
      message: 'Server error 500'
    })
  }

}
