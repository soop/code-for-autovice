require('dotenv').config()
const model_user = require('../../models/user/model-users')
const model_PRT = require('../../models/auth/model-passwordResetTokens')

const crypto = require('crypto')
const bcrypt = require('bcryptjs')
const time = require('../../helpers/time')
const mailgun = require('../../helpers/mailgun')

exports.newPasswordFromToken = async (req, res, next) => {

  try {

    const query = {
      token: req.body.token,
      user_id: req.body.user_id
    }

    const resetToken = await model_PRT.findOne(query)

    if (!resetToken) {
      return res.status(401).json({
        status: 100,
        message: 'It looks like the token you are looking for is missing'
      })
    } else {

      if (!time.isAfter(time.now(), time.hourFrom(resetToken.createdAt))) {

        const update = {
          password: await bcrypt.hash(req.body.newPassword, 15)
        }

        const user = await model_user.findByIdAndUpdate(req.body.user_id, update)

        if (user) {

          await model_PRT.deleteOne(query)

          return res.status(200).json({
            status: 200,
            message: 'Password has been updated'
          })
        }

      } else {
        return res.status(100).json({
          status: 100,
          message: 'The token you are trying to use has expired'
        })
      }
    }


  } catch (error) {
    console.log(error)
    return res.status(200).json({
      status: 500,
      message: 'something went wrong'
    })
  }

}

exports.passwordResetLink = async (req, res, next) => {

  try {

    const query = {
      email: req.body.email
    }

    const user = await model_user.findOne(query)

    console.log(user)


    if (!user) {
      console.log('no user found')
      return res.status(200).json({
        status: 200,
        message: 'If the user is registered, then we\'ve sent a mail with a reset link'
      })
    } else {


      const query = {
        token: crypto.randomBytes(42).toString('hex'),
        createdAt: time.now(),
        user_id: user._id
      }

      const token = await model_PRT.create(query)

      if (!token) {
        return res.status(500).json({
          status: 500,
          message: 'Something went wrong'
        })
      } else {

        mailgun.send('passwordReset', {
          target: user.email,
          subject: 'Password Reset',
          text: 'Password reset link',
          params: { name: user.firstName, resetLink: `${process.env.CLIENTURL}/passwordreset/${user._id}/${token.token}` }
        })

        return res.status(200).json({
          status: 200,
          message: 'If the user is registered, then we\'ve sent a mail with a reset link'
        })
      }
    }

  } catch (error) {
    console.log(error)
    return res.status(500).json({
      status: 500,
      message: 'Something went wrong'
    })
  }

}


