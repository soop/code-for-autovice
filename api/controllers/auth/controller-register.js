const bcrypt = require('bcryptjs');

const model_users = require('../../models/user/model-users');

exports.registerUser = async (req, res, next) => {

  // const userIp = req.header('x-forwarded-for') || req.connection.remoteAddress;

  try {

    if (!req.body.email && !req.body.password && !req.body.name) {
      return res.status(400).json({
        status: 400,
        message: `Email and or password is missing information`
      })
    }

    const userObj = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
    };

    if (user = await model_users.create(userObj)) {

      const token = await user.generateAuthToken();

      res.status(200).json({
        status: 200,
        message: `User was created succesfully`,
        count: user.length,
        token: token
      });
    }

  } catch (error) {
    console.log(error);

    res.status(500).json({
      status: 500,
      message: `Server error 500`
    });
  }

}

exports.companySignupUser = async (item) => {

  try {

    if (user = await model_users.create(item)) {

      return user

    } else {
      return false
    }

  } catch (error) {
    console.log(error);

    return false;
  }

}
