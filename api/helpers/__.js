module.exports = {
  arrayToArrayObject: (array, type) => {
    let res = [];

    switch (type) {
      default:
      case 'genre':
        for (let i = 0; array.length > i; i++) {
          res.push({ genreType: array[i] });
        }
        break;
      case 'category':
        for (let i = 0; array.length > i; i++) {
          res.push({ categoryType: array[i] });
        }
        break;
    }

    return res;
  },
  categorynumberToName: (string) => {
    let res = '';
    switch (string) {
      default:
      case '0001':
        res = 'theatre';
        break;
      case '0002':
        res = 'music'
        break;
      case '0003':
        res = 'museum';
        break;
    }

    return res
  },
  creditsFormat: (array) => {
    let res = [];

    for (let i = 0; array.length > i; i++) {
      let tmp = array[i].split(',');

      res.push({ title: tmp[0], person: tmp[1] });
    }

  }
}
