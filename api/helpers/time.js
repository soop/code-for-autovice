const moment = require('moment');

const normalize = async (time, format) => {

  const m = moment(time, format);

  return m().utc().toISOString();
}
exports.normalize = normalize;

/**
 * returns a timestamp of now in utc ISO string
 */
const now = () => {

  return moment().utc().toISOString();
}
exports.now = now;

/**
 * Add 1 hour to the timestamp
 * @param {*} timestamp takes 1 UTC timestamp
 */
const hourFrom = (timestamp) => {
  const time = moment.utc(timestamp)

  return time.add(1, 'h').toISOString()
}
exports.hourFrom = hourFrom;

const isAfter = (timestamp, timestamp2) => {

  return moment.utc(timestamp).isAfter(timestamp2)
}
exports.isAfter = isAfter;

const addTime = (timestamp, time, type) => {

  let m = moment.utc(timestamp)

  let res = false

  switch (type) {
    case 'm':
      m.add
      break
  }

  return res
}
