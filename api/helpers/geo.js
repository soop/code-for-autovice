
const mapBaseUrl = `https://api.mapbox.com`;
const mapBoxToken = `pk.eyJ1Ijoid2VhcG9uaXplZGxlZ28iLCJhIjoiY2s5YWsxOThtMGRqdDNmanl0cncxcWs5eCJ9.vTk0oAcELwKCqmdrk3-0HQ`;
const axios = require('axios');
const mailgun = require('./mailgun');

/**
 * This will return relevant country code
 *  This will automatically lowercase passed params.
 * @param {*} country Country in full writing exampel: Denmark
 */
const countryCode = async (country) => {

  try {
    const countryCodes = {
      Denmark: `DK`,
      Sweden: `SWE`,
      Norway: `No`
    }

    let element = false;

    switch (country.toLowerCase()) {
      default:
      case 'denmark':
        element = countryCodes.Denmark
        break;
      case 'sweden':
        element = countryCodes.Sweden;
        break;
      case 'norway':
        element = countryCodes.Norway
        break;
    }

    return element;
  } catch (error) {
    console.log(error);
    return false
  }
}

exports.countryCode = countryCode;

/**
 * Finds and returns the first result of an address lookup and converts the address to coordinates
 * @param {*} string location string in the format Country, region, address
 */
const addressToCords = async (string, type = null, id = null) => {

  try {
    return false

    const requestUrl = `${mapBaseUrl}/geocoding/v5/mapbox.places/${string}.json?access_token=${mapBoxToken}`

    let coordinates = false;

    await axios(requestUrl)
      .then(response => {
        coordinates = response.data.features[0].center
      })
      .catch(error => {
        console.log(error);
        mailgun.report(`Geocode lookup`, ``)
      })

    console.log(coordinates);

    return coordinates;
  } catch (error) {
    console.log(error);
    return false
  }
}

exports.addressToCords = addressToCords;
