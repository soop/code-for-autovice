const crypto = require('crypto');
const geo = require('./geo');

/**
 *  This function will return a venue ID based on passed params
 * @param {*} data should be an object containing zip, country, and array of venueTypes
 */
const generate = async (data) => {

  let tmp = ``;
  for (let i = 0; data.venueTypes.length > i; i++) {
    if (i === 0){
      tmp += `${data.venueTypes[i]}`;
    } else {
      tmp += `-${data.venueTypes[i]}`
    }
  }
  const geoCode = await geo.countryCode(data.country)


  const randStr = await crypto.randomBytes(6).toString('hex');

  return `${tmp}-${geoCode}-${data.zip}-${randStr}`;
}

exports.generate = generate;
