const statusTypes = {
  active: {
    string: "active",
    type: "1"
  },
  inactive: {
    string: "inactive",
    type: "2"
  },
  pending: {
    string: "pending",
    type: "3"
  },
  expired: {
    string: "expired",
    type: "9"
  }
}

const types = () => {
  return statusTypes;
}

exports.types = types;
