const template1 = require('../../templates/template_comfirnCompanyCreation')
const partnerSignupConfirm = require('../../templates/template_partnerSignupConfirmation')
const errorReporting = require('../../templates/template_errorReporting')
const passwordResetLink = require('../../templates/template_passwordResetLink')


const mailgun = require('mailgun-js')
const API_KEY = ''
const DOMAIN = ''
const HOST = ''
const mg = mailgun({ apiKey: API_KEY, domain: DOMAIN, host: HOST })

/**
 * @template is the desired template to use. Default is template1 and can be called with str::default or null
 * @data is the data points to fill out. all are required [target, subject, text]
 */
const send = async (string, data) => {

  const mailgun = require('mailgun-js')
  const API_KEY = ''
  const DOMAIN = 'DOMAIN'
  const HOST = 'api.eu.mailgun.net'
  const mg = mailgun({ apiKey: API_KEY, domain: DOMAIN, host: HOST })

  switch (template) {
  default:
  case null:
    template = template1.body
    break
  case 'partnerSignupConfirm':
    template = partnerSignupConfirm
    break
  case 'passwordReset':
    template = passwordResetLink
    break
  }

  console.log(data)


  const params = {
    from: '',
    to: data.target,
    subject: data.subject,
    text: data.text,
    html: template.body(data.params)
  }

  mg.messages().send(params, (error, body) => {
    console.log(`message sent to ${data.target}, ${data.subject}, using template: ${template.name}`)
    return true
  })
}

exports.send = send

const report = async (error, message) => {

  const template = errorReporting.body({message: message})

  const params = {
    from: '',
    to: '',
    subject: `Error:: ${error}`,
    html: template
  }

  mg.messages().send(params, (error, body) => {
    console.log('message sent')
    return true
  })
}

exports.report = report

// report(`geocode forward lookup`, `There was an error on venue ID ???????? when trying to lookup the address ??????`);
