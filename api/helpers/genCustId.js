const crypto = require('crypto')
const model_company = require('../models/administration/model-company')


const generate = async () => {

  return checkForExisting()
}

exports.generate = generate

const checkForExisting = async () => {

  const gen = await crypto.randomBytes(16).toString('hex')

  console.log(await model_company.findOne({ company_id: gen }))

  const call = await model_company.findOne({ company_id: gen })

  if (call  !== null || call !== undefined || call !== 'null') {
    return gen
  } else {
    console.log('condition not met, running recursion');
    checkForExisting()
  }

}

exports.checkForExisting = checkForExisting
