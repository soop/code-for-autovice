const mongoose = require('mongoose')

const countryList = mongoose.Schema({
  value: {
    type: String,
    unique: true,
    trim: true,
    require: [true, 'Remember to set a value']
  },
  print: {
    type: String,
    require: [true, 'Remember to set a label value']
  }
})

module.exports = mongoose.model('countryList', countryList)
