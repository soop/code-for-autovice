const mongoose = require('mongoose');

const passwordResetToken = mongoose.Schema({
  token: {
    type: String,
  },
  createdAt: {
    type: String
  },
  user_id: {
    type: String
  }
})

module.exports = mongoose.model('passwordResetToken', passwordResetToken)
