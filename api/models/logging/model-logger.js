const mongoose = require('mongoose');

const logger = mongoose.Schema({
  actionTime: {
    type: String
  },
  user: {
    type: String
  },
  action: {
    type: String
  },
  target: {
    type: String
  },
  targetId: {
    type: String
  },
  company: {
    type: String
  }
});

module.exports = mongoose.model('logs', logger)
