const mongoose = require('mongoose')

const venue = mongoose.Schema({
  isDeleted: {
    type: Boolean,
    required: true,
    unique: false,
    trim: false,
    default: false
  },
  venue_number: {
    type: String,
    required: [true, 'Please make sure there is a venue ID'],
    unique: false,
    trim: false
  },
  company: {
    type: String,
    default: 'TODO'
  },
  name: {
    type: String,
    required: [true, 'Please make sure there is a venue name'],
    unique: false,
    trim: false
  },
  venueTypes: {
    type: Array
  },
  stage: [
    {
      stageName: {
        type: String
      }
    }
  ],
  customerContact: {
    url: {
      type: String,
      required: false,
      unique: false,
      trim: false
    },
    email: {
      type: String
    },
    phone: {
      type: String
    }
  },
  location: {
    coordinates: {
      lat: {
        type: String
      },
      lng: {
        type: String
      }
    },
    country: {
      type: String
    },
    address: {
      type: String,
      required: true,
      unique: false,
      trim: false
    },
    zip: {
      type: String,
      required: true,
      unique: false,
      trim: false
    },
    city: {
      type: String,
      required: true,
      unique: false,
      trim: false
    },
    region: {
      type: String
    },
    municipality: {
      type: String
    }
  },
  about: {
    type: String
  },
  specialOpenHours: [
    {
      date: {
        type: String
      },
      open: {
        type: String
      },
      close: {
        type: String
      },
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    }
  ],
  openHours: {
    monday: {
      open: '',
      close: '',
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    },
    tuesday: {
      open: '',
      close: '',
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    },
    wednesday: {
      open: '',
      close: '',
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    },
    thursday: {
      open: '',
      close: '',
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    },
    friday: {
      open: '',
      close: '',
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    },
    saturday: {
      open: '',
      close: '',
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    },
    sunday: {
      open: '',
      close: '',
      priceTable: [
        {
          type: {
            type: String
          },
          value: {
            type: String
          }
        }
      ]
    }
  }
})

module.exports = mongoose.model('Venue', venue)

// TODO: add price and opentimes when we figure out how the hell that is supposed to work
