require('dotenv').config();
const crypto = require('crypto')
const mongoose = require('mongoose');

const companySchema = mongoose.Schema({
  isDeleted: {
    type: Boolean,
    required: true,
    unique: false,
    trim: false,
    default: false
  },
  customer_id: {
    type: String,
    required: [true, 'A customer ID is required and should be unique'],
    unique: false,
    trim: false
  },
  companyName: {
    type: String,
    required: [true, 'A name is required'],
    unique: false,
    trim: false
  },
  cvr: {
    type: String,
    default: null,
  },
  adminUser: {
    type: String
  },
  location: {
    address: {
      type: String
    },
    zip: {
      type: String
    },
    country: {
      type: String
    },
    region: {
      type: String
    },
    municipality: {
      type: String
    }
  },
  contacts: [
    {
      user_id: {
        type: String
      },
      userName: {
        type: String
      }
    }
  ],
  venues: [
    {
      venueid: {
        type: String
      }
    }
  ]
});

module.exports = mongoose.model('company', companySchema)
