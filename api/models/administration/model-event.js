const mongoose = require('mongoose')

const event = mongoose.Schema({
  isDeleted: {
    type: Boolean,
    required: true,
    unique: false,
    trim: false,
    default: false
  },
  company: {
    type: String
  },
  venue_id: {
    type: String
  },
  title: {
    type: String
  },
  subdivision: {
    type: String
  },
  description: {
    type: String
  },
  duration: {
    type: String
  },
  intermission: {
    type: Boolean,
    default: false
  },
  intermissionTime: {
    type: String,
    default: ''
  },
  totalPlayingTime: {
    type: String,
  },
  genre: [
    {
      genreType: {
        type: String
      }
    }
  ],
  ageLimit: {
    type: String
  },
numberedSeats: {
    type: Boolean,
    default: false
  },
  image: [
    {
      imageName: {
        type: String
      },
      imagePath: {
        type: String
      }
    }
  ],
  category: [
    {
      categoryType: {
        type: String
      }
    }
  ],
  credits: [
    {
      title: {
        type: String,
      },
      person: {
        type: String
      }
    }
  ],
  status: {
    type: String
  }
})

// TODO: Add price, and time / date ranges.

module.exports = mongoose.model('event', event)
