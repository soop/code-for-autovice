require('dotenv').config();
const mongoose = require('mongoose');

const contactMessage = mongoose.Schema({
  isDeleted: {
    type: Boolean,
    required: true,
    unique: false,
    trim: false,
    default: false
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  subject: {
    type: String
  },
  message: {
    type: String
  }
});

module.exports = mongoose.model('contactMessage', contactMessage);
