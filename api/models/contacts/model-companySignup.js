const mongoose = require('mongoose')

const companySignup = mongoose.Schema({
  isDeleted: {
    type: Boolean,
    required: true,
    unique: false,
    trim: false,
    default: false
  },
  cvr: {
    type: String
  },
  companyName: {
    type: String
  },
  website: {
    type: String
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  phoneNumber: {
    type: String
  },
  email: {
    type: String
  }
})

module.exports = mongoose.model('companySignup', companySignup)
