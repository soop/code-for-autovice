code-for-autovice



## Notes
Dette er det seneste kode jeg har arbejdet på, der er en smule forskel på filerne, da jeg har været igang med at install es-lint, men ikke fået converted alle filerne endnu.

Koden er en ganske simpelt API sat op til at køre i cluster mode gennem pm2 på en droplet bag en loadbalancer. Jeg har fuldt MVC patterns for at sperate concerns, og skabet
et nemt overblik over koden. Alle filer er named så de er nemme at søge på for at finde alt der har med f.eks. users, model-user, controller-user, router-user

køre med bearer authentication populated med et JWT(Json Web Token) da der ikke er nogen frontend.


Jeg har fjernet filer der indeholder følsomme data. Har dog committed config.env men slettet environment variables i config/config.env
