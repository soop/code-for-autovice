const express = require('express')

const { findVenue, insertVenue, delVenue} = require('../../api/controllers/administration/controller-venue')

const router = express.Router()

const auth = require('../../middleware/authenticate')

router
  .route('/venue')
  .get(auth, findVenue)
  .post(auth, insertVenue)


router
  .route('/venue/del')
  .post(auth, delVenue)

module.exports = router
