const express = require('express')

const { listCountries } = require('../../api/controllers/dataLists/controller-country')

const router = express.Router();

router
  .route('/countries')
  .get(listCountries)

module.exports = router

