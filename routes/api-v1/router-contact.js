const express = require('express')

const { insertContactMessage, insertCompanyContactMessage } = require('../../api/controllers/contacts/controller-contacts')

const router = express.Router();

router
  .route('/contact/message')
  .post(insertContactMessage);

router
.route('/contact/companySignup')
.post(insertCompanyContactMessage);

module.exports = router
