const express = require('express')
const event = require('./router-event')
const venue = require('./router-venue')
const company = require('./router-company')
const contact = require('./router-contact')
const data = require('./router-data')

const router = express.Router()

router.use('/v1', event)
router.use('/v1', venue)
router.use('/v1', company)
router.use('/v1', contact)
router.use('/v1', data)


module.exports = router
