const express = require('express');
const { findOneEvent, findAllEvents, insertEvent } = require('../../api/controllers/administration/controller-event');

const auth = require('../../middleware/authenticate')

const router = express.Router();

router
  .route('/event')
  .get(auth, findOneEvent)
  .post(auth, insertEvent);

router
  .route('/event/all')
  .get(auth, findAllEvents);

// router
//   .route('/filter')
//   .get(filterEvent);

  module.exports = router;
