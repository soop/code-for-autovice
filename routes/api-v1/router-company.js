const express = require('express')

const { findOneCompany, insertCompany, test } = require('../../api/controllers/administration/controller-company')

const router = express.Router();

router
  .route('/company')
  .get(findOneCompany)
  .post(insertCompany);

module.exports = router
