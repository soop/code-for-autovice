const express = require('express')
const { addCompanyVenue } = require('../../api/controllers/superUser/controller-addCompanyVenue')

const auth = require('../../middleware/authenticate')

const router = express.Router()

router
  .route('/addcompanyvenue')
  .post(auth, addCompanyVenue)


  module.exports = router
