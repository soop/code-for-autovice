const express = require('express');
const { fetchLogs, exportLogs } = require('../../api/controllers/logging/controller-logger');

const auth = require('../../middleware/authenticate');

const router = express.Router();

router
  .route('/logs')
  .get(auth, fetchLogs);

router
  .route('/export')
  .get(auth, exportLogs);

module.exports = router;
