const express = require('express')
const router = express.Router()
const api = require('./api-v1/router-api')
const auth = require('./auth/router-auth')
const logs = require('./logging/router-logger')

const superUser = require('./api-v1/router-superUser')

// server alive check should at all times answer pong
router.get('/ping', (req, res) => {
  res.status(200).send('pong')
})

router.use('/auth', auth)
router.use('/api', api)
router.use('/log', logs)

router.use('/superUser', superUser)

// 404 response
router.get('*', (req, res) => {
  res.status(404).json({
    status: 404,
    message: `page doesn't exist for a list of endpoints visit ${/* TODO: add link to documentation page*/''}`,
    path: `${req.url}`,
    link: 'https://soop.dk.dk' //TODO: link to documentation page.
  })
})


module.exports = router
