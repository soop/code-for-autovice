const express = require('express');
const { registerUser } = require('../../api/controllers/auth/controller-register');
const { login } = require('../../api/controllers/auth/controller-login');
const { findUser } = require('../../api/controllers/user/controller-user');

const auth = require('../../middleware/authenticate');

const router = express.Router();

router
  .route('/register')
  .post(registerUser);

router
  .route('/login')
  .post(login);

router
  .route('/me')
  .get(auth, findUser);

module.exports = router
