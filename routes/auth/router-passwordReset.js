const express = require('express')
const { passwordResetLink, newPasswordFromToken } = require('../../api/controllers/auth/controller-resetPassword')

const router = express.Router()

router
  .route('/reset')
  .post(passwordResetLink)

router
  .route('/newpassword')
  .post(newPasswordFromToken)

module.exports = router
