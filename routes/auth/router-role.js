const express = require('express');
const { createRole, findOneRole, findAll } = require('../../api/controllers/auth/controller-roles');

const auth = require('../../middleware/authenticate');

const router = express.Router();

router
  .route('/register')
  .post(auth, createRole);

  // TODO: Add edit role path.

  router
  .route('/find')
  .get(auth, findOneRole);

  router
  .route('/findAll')
  .get(auth, findAll);


module.exports = router
