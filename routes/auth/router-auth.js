const express = require('express')
const role = require('./router-role')
const user = require('./router-user')
const permissionTable = require('./router-permissionTable')
const passwordReset = require('./router-passwordReset')

const router = express.Router();

router.use('/role', role)
router.use('/user', user)
router.use('/permission', permissionTable)
router.use('/password', passwordReset)

module.exports = router;
