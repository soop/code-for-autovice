const express = require('express')
const { permissionTable } = require('../../api/controllers//administration/controller-permissionTable')

const auth = require('../../middleware/authenticate')

const router = express.Router();

router
.route('/permissionTable')
.get(auth, permissionTable);




module.exports = router
